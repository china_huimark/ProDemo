20170730：
1.增加采用JAX-WS实现webservie功能
2.然后 与tomcat结合，与spring结合。推荐与spring结合，方便IOC注入依赖的bean。目前，并没有合适的spring和jaxws融合的方案。
3.记录SOAP报文IN/OUT，方便对调用记录进行跟踪

20170730-1
1.返回xml，json报文配置，参考类DataFormatController
2.防止CSRF的TokenInterceptor

20170809
增加netty例子
增加SqlSessionTemplate

20170809-1
增加xml-bean互转，增加javax.xml.soap

20170810
增加jax-ws的spring整合方式SimpleJaxWsServiceExporter、SimpleJaxWsServiceExporter。\r\n
该方式只是不能和项目所在容器公用同一个端口，因为默认情况下，这种方式用另外一个HttpServer实现的