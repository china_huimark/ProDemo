package bourne.com.xstream;

import java.io.IOException;
import java.util.Date;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.junit.Test;

import bourne.com.soap.Request;
import bourne.com.soap.RequestBody;
import bourne.com.soap.RequestHeader;
import bourne.com.util.JsonXmlUtil;

public class RequestTest {

	@Test
	public void toXml() {
		RequestHeader rh = new RequestHeader();
		rh.setChannel("手机银行");
		rh.setOperNo("0014028");
		rh.setOperTerminalNo("18580060347");
		rh.setReqTimeStamp(new Date().toString());
		rh.setTxNo("00001");
		System.out.println(JsonXmlUtil.toXml(rh));
	}

	@Test
	public void toXml2() {
		RequestBody rb = new RequestBody();
		rb.setName("bourne");
		rb.setPwd("123456");
		System.out.println(JsonXmlUtil.toXml(rb));
	}

	@Test
	public void toXml3() {
		JsonXmlUtil jxu = new JsonXmlUtil();

		Request rq = new Request();

		RequestBody rb = new RequestBody();
		rb.setName("bourne");
		rb.setPwd("123456");

		RequestHeader rh = new RequestHeader();
		rh.setChannel("手机银行");
		rh.setOperNo("0014028");
		rh.setOperTerminalNo("18580060347");
		rh.setReqTimeStamp(new Date().toString());
		rh.setTxNo("00001");

		rq.setBody(rb);
		rq.setHeader(rh);

		System.out.println(JsonXmlUtil.toXml(rq));
	}

	@Test
	public void toBody() {
		String body = "<body>\r\n" + "  <name>bourne</name>\r\n" + "  <pwd>123456</pwd>\r\n" + "</body>";
		RequestBody b = JsonXmlUtil.toBean(body, RequestBody.class);
		System.out.println(b.getName());
	}

	@Test
	public void toSoapXml() throws SOAPException, IOException {
		RequestHeader rh = new RequestHeader();
		rh.setChannel("手机银行");
		rh.setOperNo("0014028");
		rh.setOperTerminalNo("18580060347");
		rh.setReqTimeStamp(new Date().toString());
		rh.setTxNo("00001");

		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage outgoingMessage = messageFactory.createMessage();
		SOAPPart soappart = outgoingMessage.getSOAPPart();
		SOAPEnvelope envelope = soappart.getEnvelope();
		SOAPHeader header = envelope.getHeader();
		SOAPBody body = envelope.getBody();
		body.addTextNode(JsonXmlUtil.toXml(rh));
		
		outgoingMessage.writeTo(System.out);
	}
}
