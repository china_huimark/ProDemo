package bourne.com.jaxws.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.FileUtils;

public class TestSoapService {
	public static void main(String[] args) throws Exception {
		String wsdl = "http://localhost:9090/SoapService?wsdl";
		int timeout = 10000;
		String content = null;
		content = FileUtils.readFileToString(new File("E:/tmp/helloworld.ws"), "UTF-8");

		// HttpURLConnection 发送SOAP请求
		System.out.println("HttpURLConnection 发送SOAP请求");
		URL url = new URL(wsdl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		System.setProperty("http.agent", "");
		conn.addRequestProperty("User-Agent", "Mozilla/4.0(compatible;MSIE5.5;Windows NT; DigExt)");
		conn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
		conn.setRequestMethod("POST");
		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setConnectTimeout(timeout);
		conn.setReadTimeout(timeout);

		DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
		dos.write(content.getBytes("utf-8"));
		dos.flush();

		BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
		String line = null;
		StringBuffer strBuf = new StringBuffer();
		while ((line = reader.readLine()) != null) {
			strBuf.append(line);
		}
		dos.close();
		reader.close();
		conn.disconnect();

		System.out.println(strBuf.toString());
	}
}
