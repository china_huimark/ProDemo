package bourne.com.jaxws.test;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * 通过UrlConnection调用Webservice服务
 *
 */
public class App2 {

	public static void main(String[] args) throws Exception {
		// 服务的地址
		URL wsUrl = new URL("http://127.0.0.1:8080/hs");

		HttpURLConnection conn = (HttpURLConnection) wsUrl.openConnection();

		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setUseCaches(false);
		conn.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");

		OutputStream os = conn.getOutputStream();

		// 请求体
		String soap = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:q0=\"http://www.cqboure.com/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
				+ "<soapenv:Body> <q0:sayHello><arg0>aaa</arg0>  </q0:sayHello> </soapenv:Body> </soapenv:Envelope>";

		DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
		dos.write(soap.getBytes("utf-8"));
		dos.flush();

		BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
		String line = null;
		StringBuffer strBuf = new StringBuffer();
		while ((line = reader.readLine()) != null) {
			strBuf.append(line);
		}
		dos.close();
		reader.close();
		conn.disconnect();

		System.out.println(strBuf.toString());
	}
}