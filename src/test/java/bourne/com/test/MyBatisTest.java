package bourne.com.test;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import bourne.com.dao.GeneralDao;
import bourne.com.entity.User;
import bourne.com.service.IUserService;

public class MyBatisTest {
	private GeneralDao dao;

	/**
	 * 这个before方法在所有的测试方法之前执行，并且只执行一次 所有做Junit单元测试时一些初始化工作可以在这个方法里面进行
	 * 比如在before方法里面初始化ApplicationContext和userService
	 */
	@Before
	public void before() {
		// 使用"spring.xml"和"spring-mybatis.xml"这两个配置文件创建Spring上下文
		ApplicationContext ac = new ClassPathXmlApplicationContext(
				new String[] { "applicationContext.xml", "spring-mybatis.xml" });
		// 从Spring容器中根据bean的id取出我们要使用的userService对象
		dao = (GeneralDao) ac.getBean("generalDaoImpl");
	}

	@Test
	public void insert() {
		User user = new User();
		user.setAge(28);
		user.setName("bourne");
		dao.insert(user);
	}

}
