package bourne.com.test;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @Description: 测试转义字符的存储、按行导出txt、按行读取 问题
 * @author bourne
 * @date 2017年11月16日-下午10:30:34
 * @version V1.0
 */
public class StringTest {

	public static void main(String[] args) throws IOException {
		String documentTxt = "abc\rdef";
		System.out.println(documentTxt);// .replaceAll("\n", "\\\\n")

		System.out.println("hello \\r\\n world");
		// File file = new File("E:/tmp/rn.txt");
		// FileUtils.writeStringToFile(file, documentTxt, "utf-8");

		// 模拟 前段输入，包含 换行符 和 分隔符（预定义）
		String s1 = "S1";
		String s2 = "S2\n2";
		String s3 = "~S3";
		// 预定义分隔符
		String sp = "~";
		// 处理换行符-unix，目标就是 将输入原样存入 TXT
		s2 = s2.replaceAll("\n", "\\\\n");
		// 处理分隔符，在前面价格\
		s3 = s3.replace("~", "\\~");
		String res = s1 + sp + s2 + sp + s3;
		System.out.println(res);

		//模拟从TXT按行读取，并split得到每个字段的内容
		Pattern pattern = Pattern.compile("(?<!\\\\)~", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher("S1~S2\\n2~\\~S3");
		// using Matcher find(), group(), start() and end() methods
		int start = 0;
		while (matcher.find()) {
			System.out.println("Found the text \"" + matcher.group() + "\" starting at " + matcher.start()
					+ " index and ending at index " + matcher.end());
			System.out.println("sub=" + res.substring(start, matcher.start()));
			start = matcher.end();
		}
		System.out.println("sub=" + res.substring(start));
	}

}
