<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%--引入JSTL核心标签库 --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>防重复提交测试</title>
<script type="text/javascript">
	$.ajax({
		type : "post",
		dataType : "html",
		url : 'saveData',
		data : {token:xx},
		success : function(data) {
			if (data != "") {
				$("#pager").pager({
					pagenumber : pagenumber,
					pagecount : data.split("$$")[1],
					buttonClickCallback : PageClick
				});
				$("#anhtml").html(data.split("$$")[0]);

			}
		}
	});
</script>
</head>
<body>
	<form action='saveData'>
		u1:${map.n1}
		<hr />
		u1:${map.n2} <input type="hidden" name="token" value="${token}" /> <input
			type="submit" value="提交" />
	</form>
</body>
</html>