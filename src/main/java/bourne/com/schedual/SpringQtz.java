package bourne.com.schedual;

import java.util.Date;

public class SpringQtz {
	private static int counter = 0;

	protected void execute() {
		System.out.println("\t\t" + new Date());
		System.out.println("(" + counter++ + ")");
	}
}
