package bourne.com.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bourne.com.dao.GeneralDao;
import bourne.com.dao.UserMapper;
import bourne.com.dao.impl.BaseDaoImpl;
import bourne.com.entity.User;
import bourne.com.service.IUserService;

@Service("userService")
public class UserServiceImpl implements IUserService {
	/**
	 * 使用@Autowired注解标注userMapper变量， 当需要使用UserMapper时，Spring就会自动注入UserMapper
	 */
	@Autowired
	private GeneralDao dao;// 注入dao

	public User getUserById(int userId) {
		// TODO Auto-generated method stub
		return (User) this.dao.selectByPrimaryKey(userId);
	}

	public void addUser(User user) {
		this.dao.insert(user);
	}

}
