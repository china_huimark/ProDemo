package bourne.com.service;

import bourne.com.entity.User;

public interface IUserService {

	public User getUserById(int userId);

	void addUser(User user);

}