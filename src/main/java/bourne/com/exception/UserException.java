package bourne.com.exception;

//这里不要用RuntimeException
//因为RuntimeException会导致服务端在抛异常给客户端时,服务端自身也会抛相同的异常
//所以WebServices开发中定义异常时要注意这一点
public class UserException extends Exception {
	private static final long serialVersionUID = 6252203957834273236L;

	public UserException() {
		super();
	}

	public UserException(String message) {
		super(message);
	}
}
