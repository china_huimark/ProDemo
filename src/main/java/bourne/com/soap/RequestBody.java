package bourne.com.soap;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("body")
public class RequestBody {

	private String name;
	private String pwd;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

}
