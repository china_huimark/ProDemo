package bourne.com.soap;

import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

public class DemoSend {

	public static void main(String[] args) throws ServletException, IOException {
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
			SOAPMessage outgoingMessage = messageFactory.createMessage();
			SOAPPart soappart = outgoingMessage.getSOAPPart();
			SOAPEnvelope envelope = soappart.getEnvelope();
			SOAPHeader header = envelope.getHeader();
			SOAPBody body = envelope.getBody();

//			body.addBodyElement(envelope.createName("numberAvailable", "laptops", "http://ecodl.taobao.com/"))
//					.addTextNode("216");
			
			body.addTextNode("444");
			outgoingMessage.writeTo(System.out);

		} catch (SOAPException e) {
			e.printStackTrace();
		}

	}

}
