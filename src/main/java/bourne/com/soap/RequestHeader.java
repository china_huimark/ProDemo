package bourne.com.soap;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * 
 * @Description: 请求报文头，描述请求的基本信息
 * @author bourne
 * @date 2017年8月9日-下午11:16:21
 * @version V1.0
 */
@XStreamAlias("header")
public class RequestHeader {
	@XStreamAsAttribute()
	private String version = "1.0";
	// 渠道号
	private String channel;
	// 交易员号
	private String operNo;
	// 交易终端号
	@XStreamAlias("operTerNo")
	private String operTerminalNo;
	// 请求交易码
	private String txNo;
	// 请求时间戳
	private String reqTimeStamp;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getOperNo() {
		return operNo;
	}

	public void setOperNo(String operNo) {
		this.operNo = operNo;
	}

	public String getOperTerminalNo() {
		return operTerminalNo;
	}

	public void setOperTerminalNo(String operTerminalNo) {
		this.operTerminalNo = operTerminalNo;
	}

	public String getTxNo() {
		return txNo;
	}

	public void setTxNo(String txNo) {
		this.txNo = txNo;
	}

	public String getReqTimeStamp() {
		return reqTimeStamp;
	}

	public void setReqTimeStamp(String reqTimeStamp) {
		this.reqTimeStamp = reqTimeStamp;
	}
}
