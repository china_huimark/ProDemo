package bourne.com.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import bourne.com.interceptor.Token;

@Controller
@RequestMapping("/tc")
public class TokenController {

	@RequestMapping("show")
	@Token(save = true)
	public String show(HttpServletRequest request, HttpServletResponse response) {
		Map map = new HashMap();

		map.put("n1", "huimark");
		map.put("n2", "bourne");

		request.setAttribute("map", map);
		return "token/index";

	}

	@RequestMapping("saveData")
	@ResponseBody
	@Token(remove = true)
	public String saveData(HttpServletRequest request, HttpServletResponse response) {
		return "xxx";
	}
}
