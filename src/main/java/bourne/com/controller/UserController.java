package bourne.com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import bourne.com.dao.GeneralDao;
import bourne.com.entity.User;

@Controller
@RequestMapping("/user")
public class UserController {

	private static final long serialVersionUID = 5880583654747599704L;

	@Autowired
	private GeneralDao dao;// 注入dao

	@RequestMapping("/index")
	public String index(Model model) {
		List<User> lstUsers = (List<User>) dao.getAll(new User());
		model.addAttribute("lstUsers", lstUsers);
		return "/index";
	}

}