package bourne.com.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import bourne.com.controller.vo.Book;

@Controller
@RequestMapping("/dfc")
public class DataFormatController {

	@RequestMapping(value = "json", produces = { "application/json;charset=UTF-8" })
	@ResponseBody
	public Book showJson(HttpServletRequest request, HttpServletResponse response) {
		Book b = new Book();
		b.setAuthor("bourne");
		b.setName("java programming");
		b.setDatetime("20170721");
		return b;
	}

	@RequestMapping(value = "xml", produces = { "application/xml;charset=UTF-8" })
	@ResponseBody
	public Book showXml(HttpServletRequest request, HttpServletResponse response) {
		Book b = new Book();
		b.setAuthor("bourne");
		b.setName("java programming");
		b.setDatetime("20170721");
		return b;
	}
}
