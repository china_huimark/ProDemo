package bourne.com.dao;

import java.util.List;

public interface GeneralDao {
	int deleteByPrimaryKey(Integer id);

	int insert(Object entity);

	int insertSelective(Object entity);

	Object selectByPrimaryKey(Integer id);

	int updateByPrimaryKeySelective(Object entity);

	int updateByPrimaryKey(Object entity);

	List<?> getAll(Object entity);
}
