package bourne.com.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Component;

import bourne.com.dao.GeneralDao;

@Component
public class GeneralDaoImpl implements GeneralDao {

	protected static final String SQL_INSERT = ".insert";
	protected static final String SQL_SELECTLIST = ".insert";

	@Resource(name = "sqlSessionTemplate")
	private SqlSessionTemplate sqlSessionTemplate;

	@Override
	public int deleteByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insert(Object o) {
		return sqlSessionTemplate.insert(o.getClass().getName() + SQL_INSERT, o);
	}

	@Override
	public int insertSelective(Object entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Object selectByPrimaryKey(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int updateByPrimaryKeySelective(Object entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByPrimaryKey(Object entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<?> getAll(Object entity) {
		return sqlSessionTemplate.selectList(entity.getClass().getName() + SQL_INSERT, entity);
	}

}
