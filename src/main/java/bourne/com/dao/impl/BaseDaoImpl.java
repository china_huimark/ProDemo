package bourne.com.dao.impl;

import java.io.Serializable;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BaseDaoImpl<PK extends Serializable> extends SqlSessionDaoSupport implements Serializable {

	private static final long serialVersionUID = 7623507504198633838L;

	private final String PREFIX = "bourne.com.dao.";
	private final String POSTFIX = "Mapper";

	private final String _INSERT = ".insert";

	private final String _INSERTSELECTIVE = ".insertSelective";

	private final String _SELECTBYPRIMARYKEY = ".selectByPrimaryKey";

	private final String _UPDATEBYPRIMARYKEY = ".updateByPrimaryKey";

	private final String _UPDATEBYPRIMARYKEYSELECTIVE = ".updateByPrimaryKeySelective";

	private final String _DELETEBYPRIMARYKEY = ".deleteByPrimaryKey";

	@Autowired
	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		super.setSqlSessionFactory(sqlSessionFactory);
	}

	public String getNampSpace(Object object) {
		String simpleName = PREFIX + object.getClass().getSimpleName() + POSTFIX;
		return simpleName;
	}

	public int insert(Object entity) {
		return getSqlSession().insert(this.getNampSpace(entity) + _INSERT, entity);
	}

	// public int insertSelective(Object entity) {
	// return getSqlSession().insert(this.getNampSpace(entity) + _INSERTSELECTIVE,
	// record);
	// }

	// public T selectByPrimaryKey(Object entity,PK id) {
	// return getSqlSession().selectOne(this.getNampSpace(entity) +
	// _SELECTBYPRIMARYKEY, id);
	// }
	//
	// public int updateByPrimaryKey(T record) {
	// return getSqlSession().update(this.getNampSpace(entity) +
	// _UPDATEBYPRIMARYKEY, record);
	// }
	//
	// public int updateByPrimaryKeySelective(T record) {
	// return getSqlSession().update(this.getNampSpace(entity) +
	// _UPDATEBYPRIMARYKEYSELECTIVE, record);
	// }
	//
	// public int deleteByPrimaryKey(PK id) {
	// return getSqlSession().delete(this.getNampSpace(entity) +
	// _DELETEBYPRIMARYKEY, id);
	// }

}
