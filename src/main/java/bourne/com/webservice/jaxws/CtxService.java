package bourne.com.webservice.jaxws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

@WebService(targetNamespace = "http://blog.csdn.net/jadyer")
public interface CtxService {

	@WebResult(name = "sayHelloResult")
	public String sayHello(@WebParam(name = "name") String name);
}
