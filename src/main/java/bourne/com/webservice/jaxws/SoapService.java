package bourne.com.webservice.jaxws;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import bourne.com.exception.UserException;

@WebService(targetNamespace = "http://blog.csdn.net/jadyer")
public interface SoapService {
	@WebResult(name = "sayHelloResult")
	public String sayHello(@WebParam(name = "name") String name);

	@WebResult(name = "loginResult")
	public String login(@WebParam(name = "username") String username, @WebParam(name = "password") String password)
			throws UserException;
}