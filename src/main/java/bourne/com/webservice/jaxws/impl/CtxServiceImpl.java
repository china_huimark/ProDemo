package bourne.com.webservice.jaxws.impl;

import javax.jws.HandlerChain;
import javax.jws.WebService;

import org.springframework.stereotype.Component;

import bourne.com.webservice.jaxws.CtxService;

@Component
@WebService(serviceName = "ctxService", endpointInterface = "bourne.com.webservice.jaxws.CtxService", targetNamespace = "http://blog.csdn.net/jadyer")
@HandlerChain(file = "wsHandlerChain.xml")
public class CtxServiceImpl implements CtxService {

	@Override
	public String sayHello(String name) {
		System.out.println("Receive the name=[" + name + "]......");
		if (null == name) {
			return "Hello,World";
		} else {
			return "Hello," + name;
		}
	}
}
