package bourne.com.webservice.jaxws.impl;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;

import org.springframework.stereotype.Component;

import bourne.com.exception.UserException;
import bourne.com.webservice.jaxws.SoapService;

/**
 * 
 * @Description: HandlerChain文件路径放置到classpath即可。实际开发时，每个webservice都要继承一个基础类来保证@HandlerChain的存在。
 *               不能配置为默认handler？通过以下方法获取request HttpServletRequest request =
 *               (HttpServletRequest)context.get(AbstractHTTPDestination.HTTP_REQUEST);
 * @author bourne
 * @date 2017年7月25日-下午11:29:00
 * @version V1.0
 */
@WebService(serviceName = "soapService", endpointInterface = "bourne.com.webservice.jaxws.SoapService", targetNamespace = "http://blog.csdn.net/jadyer")
@HandlerChain(file = "wsHandlerChain.xml")
public class SoapServiceImpl implements SoapService {

	@Override
	public String sayHello(String name) {
		System.out.println("Receive the name=[" + name + "]......");
		if (null == name) {
			return "Hello,World";
		} else {
			return "Hello," + name;
		}
	}

	@Override
	public String login(String username, String password) throws UserException {
		System.out.println("Receive the username=[" + username + "],password=[" + password + "]......");
		if ("admin".equals(username) && "hongyu".equals(password)) {
			return "用户[" + username + "]认证通过";
		}
		throw new UserException("用户[" + username + "]认证未通过");
	}

	public static void main(String[] args) {
		System.out.println("server9090 is running");
		String address = "http://localhost:9090/SoapService";
		SoapService implementor = new SoapServiceImpl();
		Endpoint.publish(address, implementor);
	}
}
